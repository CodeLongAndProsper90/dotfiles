import sys
import argparse
import json
parser = argparse.ArgumentParser()
parser.add_argument('-e', nargs='*')
parser.add_argument('-c')
args = parser.parse_args()
stdin = sys.stdin.read()
data  = json.loads(stdin)

if not args.c:
    for window in data:
        if not args.e or window['name'] not in args.e:
            print(window['name'])
else:
    focused = [win for win in data if win['visible']]
    print(focused[0]['name'])
