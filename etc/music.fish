set prev (mpc | head -n1)
while true
	if test "$prev" != (mpc | head -n1) # We have a new song
		
		set artist (mpc | head -n1 | cut -d- -f1)
		set song (mpc | head -n1 | cut -d- -f2- | xargs)
		notify-send "Now Playing:" "$artist\n$song" -c musicalert
	end
	set prev (mpc | head -n1)
	sleep 0.5
end

