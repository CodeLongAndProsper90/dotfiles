Ok so
These are my config files
To set them up, you need [chezmoi](https://chezmoi.io) installed.

After that, clone this repo:
```
chezmoi init https://gitlab.com/CodeLongAndProsper90/dotfiles
```
and view the changes:
```
chezmoi apply -nv
```
if that's good, apply
```
chezmoi apply
```
But it overwrites your current config, ok?
